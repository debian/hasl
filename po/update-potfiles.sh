#!/bin/sh -e

# make sure we're in the directory that contains this file.
PARENT=$(dirname "${0}")
cd "${PARENT}"

# Find all of the source files in the library directory, get their path
# relative to the parent directory of this file, sort them all, and then write
# them to POTFILES and output to stdout.
find ../hasl -iname "*.c" -exec realpath --relative-to .. {} \; | sort | tee POTFILES

